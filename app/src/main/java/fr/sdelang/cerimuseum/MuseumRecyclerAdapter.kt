package fr.sdelang.cerimuseum

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewStub
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import fr.sdelang.cerimuseum.data.MuseumObject
import fr.sdelang.cerimuseum.data.MuseumRepository

class MuseumRecyclerAdapter :
        RecyclerView.Adapter<MuseumRecyclerAdapter.ViewHolder>() {

    var objects: List<MuseumObject> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var categoryAnchors: Map<Int, String> = HashMap()

    class ViewHolder(view: View, private val adapter: MuseumRecyclerAdapter) : RecyclerView.ViewHolder(view) {
        val categoryHeaderTitle: TextView = view.findViewById(R.id.objectCategoryHeaderTitle)
        val categoryHeader: View = view.findViewById(R.id.objectCategoryHeader)
        val thumbnail: ImageView = view.findViewById(R.id.objectThumbnail)
        val name: TextView = view.findViewById(R.id.objectName)
        val description: TextView = view.findViewById(R.id.objectDescription)
        val tags: TextView = view.findViewById(R.id.objectTags)
        val popularity: TextView = view.findViewById(R.id.objectPopularity)
        private val card: CardView = view.findViewById(R.id.objectCard)

        init {
            card.setOnClickListener {
                val action = MuseumListFragmentDirections.actionMuseumObjectListToMuseumDetailFragment(
                    adapter.objects[adapterPosition].info.itemID
                )
                view.findNavController().navigate(action)
            }
        }
    }

    private fun getRowLayout(viewType: Int): Int {
        return when (viewType) {
            0 -> R.layout.museum_object_row
            else -> R.layout.museum_object_row_odd
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.museum_object_card, parent, false)

        val stub: ViewStub = view.findViewById(R.id.museum_object_row_stub)
        stub.layoutResource = getRowLayout(viewType)
        stub.inflate()

        return ViewHolder(view, this)
    }

    /**
     * @return Depending on the item position in the list, returns:
     * - 0 for an even row
     * - 1 for an uneven row
     */
    override fun getItemViewType(position: Int): Int = position % 2

    override fun getItemCount() = objects.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val obj = objects[position]
        Picasso.get().load(MuseumRepository.instance.getThumbnailURL(obj.info.itemID)).into(holder.thumbnail)

        val anchor = categoryAnchors[position]

        if (anchor != null) {
            holder.categoryHeader.visibility = View.VISIBLE
            holder.categoryHeaderTitle.text = anchor
        } else {
            holder.categoryHeader.visibility = View.GONE
        }

        val context: Context = MainActivity.instance
        if (obj.info.brand.isNullOrBlank()) {
            holder.name.text = context.getString(R.string.preview_title_no_brand, obj.info.name)
        } else {
            holder.name.text = context.getString(R.string.preview_title_brand, obj.info.brand, obj.info.name)
        }

        holder.description.text = obj.info.description

        holder.tags.text = context.getString(
                R.string.preview_tags,
                obj.info.categories.joinToString(separator = ", ")
        )

        holder.popularity.text = if (obj.popularity != null) context.getString(
                if (obj.localVote?.isFavorite == true) R.string.preview_popularity_favorited
                else R.string.preview_popularity,
                obj.popularity.popularity
        ) else null
    }
}