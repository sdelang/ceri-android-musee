package fr.sdelang.cerimuseum

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        instance = this
    }

    companion object {
        lateinit var instance: Activity private set
    }
}

// NÉCESSAIRE:

// autres, ordre décroissant
// TODO: toolbar detail
// TODO: refresh individuel?
// TODO: stocker la version dans la Room au lieu de reload à chaque fois
// TODO: migration du kotlin-extensions
// TODO: intent image
// TODO: documenter ~toutes les fonctions
// TODO: gestion erreur repository à rendre moins cassé (tester en mode avion)
// TODO: warnings à corriger