package fr.sdelang.cerimuseum.data.webservice

import fr.sdelang.cerimuseum.data.MuseumObjectInfo
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * Defines the museum API interface to be implemented by Retrofit.
 */
interface MuseumInterface {
    /**
     * Gets a list of all museum object categories.
     * Note that this is unused in its current state, because only
     * [fr.sdelang.cerimuseum.MuseumListViewModel.getCategories] really requires using categories,
     * and infers this from the object list rather than from the API list.
     *
     * @see MuseumObjectInfo
     */
    @GET("categories")
    fun getCategories(): Call<Array<String>>

    /**
     * Gets a map of all museum objects.
     *
     * [MuseumObjectInfo.itemID] is not populated by this method.
     *
     * @return A map, where the [String] key is the item ID of a museum object, and the
     * [MuseumObjectInfo] value is the object itself.
     */
    @GET("collection")
    fun getCollection(): Call<Map<String, MuseumObjectInfo>>

    /**
     * Gets a version identifier for the latest collection available remotely.
     */
    @GET("collectionversion")
    fun getCollectionVersion(): Call<String>

    /*
    /**
     * Get an object with a specific itemID.
     */
    @GET("items/{itemID}")
    fun getObject(@Path("itemID") itemID: String): Call<MuseumObjectInfo>*/

    /**
     * Gets the thumbnail for an item.
     * Note that Retrofit is not used to make the request itself, only to compute the URL.
     *
     * @see fr.sdelang.cerimuseum.data.MuseumRepository.getThumbnailURL
     */
    @GET("items/{itemID}/thumbnail")
    fun getThumbnail(@Path("itemID") itemID: String): Call<ResponseBody>

    /**
     * Gets a picture of item ID [itemID] and image ID [imageID].
     * Note that REtrofit is not used to make the request itself, only to compute the URL.
     *
     * @see fr.sdelang.cerimuseum.data.MuseumRepository.getPictureURL
     */
    @GET("items/{itemID}/images/{imageID}")
    fun getPicture(@Path("itemID") itemID: String, @Path("imageID") imageID: String): Call<ResponseBody>

    /**
     * Gets a map of all popularity info.
     *
     * @return A map, where the [String] key is the item ID of a museum object, and the [Int] value
     * is the amount of people who have added the item to their favorites. When that amount is zero,
     * the pair will be missing.
     */
    @GET("popularity")
    fun getPopularity(): Call<Map<String, Int>>

    /**
     * Gets a version identifier for the latest popularity info available remotely.
     *
     * @see getCollectionVersion
     */
    @GET("popularityversion")
    fun getPopularityVersion(): Call<String>

    /**
     * Like a museum object of item ID [itemID].
     *
     * Liking/disliking should be seen as adding/removing a museum object from one's favorites.
     * However, the API does not perform any sort of authentication, and as such it is needed to
     * track whether an item has been added to favorites already.
     *
     * This can, of course, be abused.
     *
     * @see dislike
     * @see fr.sdelang.cerimuseum.data.MuseumObjectLocalState
     */
    @POST("items/{itemID}/like")
    fun like(@Path("itemID") itemID: String): Call<ResponseBody>

    /**
     * Dislike a museum object of item ID [itemID].
     *
     * @see like
     */
    @POST("items/{itemID}/dislike")
    fun dislike(@Path("itemID") itemID: String): Call<ResponseBody>
}