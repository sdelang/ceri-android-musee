package fr.sdelang.cerimuseum.data.database

import androidx.lifecycle.LiveData
import androidx.room.*
import fr.sdelang.cerimuseum.data.MuseumObject
import fr.sdelang.cerimuseum.data.MuseumObjectInfo
import fr.sdelang.cerimuseum.data.MuseumObjectLocalState
import fr.sdelang.cerimuseum.data.MuseumObjectPopularity

@Dao
interface MuseumDAO {
    /**
     * Get the list of museum objects using the MuseumObject type which aggregates all known data
     * for a museum object.
     *
     * @return A LiveData to an unsorted list of museum objects.
     */
    @Transaction
    @Query("SELECT museum_objects.* FROM museum_objects")
    fun getAllObjects(): LiveData<List<MuseumObject>>

    /**
     * Get an object info with a specific itemID.
     */
    @Query("SELECT * FROM museum_objects WHERE itemID=:itemID")
    fun getObject(itemID: String): LiveData<MuseumObject>

    /**
     * Insert object infos. Does not delete any removed ones.
     *
     * @see reassignObjectInfos
     */
    @Insert
    fun insertObjectInfos(vararg obj: MuseumObjectInfo)

    /**
     * Delete all object infos.
     *
     * @see reassignObjectInfos
     */
    @Query("DELETE FROM museum_objects")
    fun deleteAllObjectInfos()

    /**
     * Delete all object infos and insert a new set of object infos.
     * This is generally useful when you want to update the entire object info database with data
     * obtained from an API.
     * This is done as a single transaction to avoid list flickering or other consistency issues.
     *
     * @param obj The new set of object infos to store.
     */
    @Transaction
    fun reassignObjectInfos(vararg obj: MuseumObjectInfo) {
        deleteAllObjectInfos()
        insertObjectInfos(*obj)
    }

    /**
     * Insert popularity for multiple objects. Does not delete any removed ones.
     *
     * @see reassignPopularity
     */
    @Insert
    fun insertPopularity(vararg obj: MuseumObjectPopularity)

    /**
     * Delete all object popularity.
     *
     * @see reassignPopularity
     */
    @Query("DELETE FROM museum_object_popularity")
    fun deleteAllPopularity()

    /**
     * Delete all object popularity info and insert a new set of popularity info.
     * This is generally useful when you want to update the entire popularity info database with
     * data obtained from an API.
     * This is done as a single transaction to avoid list flickering or other consistency issues.
     *
     * @param obj The new set of object popularity info to store.
     */
    @Transaction
    fun reassignPopularity(vararg obj: MuseumObjectPopularity) {
        deleteAllPopularity()
        insertPopularity(*obj)
    }

    /**
     * Insert new local state or replace existing local state.
     *
     * @see getLocalState
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLocalState(vote: MuseumObjectLocalState)

    /**
     * Get local state for a museum object of a specific itemID.
     *
     * @return A potentially null local state object.
     */
    @Transaction
    @Query("SELECT * FROM museum_object_local_state WHERE itemID=:itemID")
    fun getLocalState(itemID: String): LiveData<MuseumObjectLocalState?>
}