package fr.sdelang.cerimuseum.data

import android.widget.Toast
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import fr.sdelang.cerimuseum.MainActivity
import fr.sdelang.cerimuseum.R
import fr.sdelang.cerimuseum.data.database.MuseumRoomDatabase
import fr.sdelang.cerimuseum.data.webservice.MuseumInterface
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.await
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.IOException

class MuseumRepository {
    val moshi = Moshi.Builder()
            .addLast(KotlinJsonAdapterFactory())
            .build()

    val retrofit = Retrofit.Builder()
            .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
            .addConverterFactory(MoshiConverterFactory.create(moshi)).build()

    val dao = MuseumRoomDatabase.instance.getMuseumDAO()

    private val api = retrofit.create(MuseumInterface::class.java)

    private val objects: LiveData<List<MuseumObject>> = dao.getAllObjects()

    private var apiObjectsVersion = ""
    private var apiVotesVersion = ""

    /*fun loadObject(itemID: String): LiveData<MuseumObject> {
        var item = MutableLiveData<MuseumObject>()

        api.getObject(itemID).enqueue(object : Callback<MuseumObject> {
            override fun onResponse(call: Call<MuseumObject>, response: Response<MuseumObject>) {
                // TODO: check for isSuccessful/null
                item.value = response.body()!!
            }

            override fun onFailure(call: Call<MuseumObject>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })

        return item
    }*/

    fun getObjects(): LiveData<List<MuseumObject>> = objects

    @WorkerThread
    private suspend fun loadObjects() {
        val latestVersion = api.getCollectionVersion().await()
        if (apiObjectsVersion == latestVersion) {
            // No update required
            return;
        }

        val ret = ArrayList<MuseumObjectInfo>()

        for ((itemID, obj) in api.getCollection().await()) {
            obj.itemID = itemID
            ret.add(obj)
        }

        dao.reassignObjectInfos(*ret.toTypedArray())
        apiObjectsVersion = latestVersion
    }

    @WorkerThread
    suspend fun loadPopularity() {
        val latestVersion = api.getPopularityVersion().await()
        if (apiVotesVersion == latestVersion) {
            // No update required
            return;
        }

        val ret = ArrayList<MuseumObjectPopularity>()

        for ((itemID, popularity) in api.getPopularity().await()) {
            ret.add(MuseumObjectPopularity(itemID, popularity))
        }

        dao.reassignPopularity(*ret.toTypedArray())
        apiVotesVersion = latestVersion
    }

    /**
     * (Re)loads museum collection data asynchronously.
     * On network failure, a toast message will be shown.
     * On a parse error, an error message will be logged.
     * @see loadObjects
     * @see loadPopularity
     */
    @WorkerThread
    suspend fun loadAll() {
        try {
            coroutineScope {
                async { loadObjects() }.start()
                async { loadPopularity() }.start()
            }
        } catch (t: IOException) {
            MainActivity.instance.runOnUiThread {
                Toast.makeText(MainActivity.instance, MainActivity.instance.getString(R.string.network_error), Toast.LENGTH_LONG).show()
            }
        } catch (t: Throwable) {
            println("Error occurred on fetch: $t")
        }
    }

    /**
     * Returns the thumbnail for a museum object
     * @see MuseumInterface.getThumbnail
     */
    fun getThumbnailURL(itemID: String): String {
        return api.getThumbnail(itemID).request().url().toString()
    }

    /**
     * Returns the URL to a picture for a museum object
     * @see MuseumInterface.getPicture
     * @see MuseumObjectInfo.pictures
     */
    fun getPictureURL(itemID: String, imageID: String): String {
        return api.getPicture(itemID, imageID).request().url().toString()
    }

    /**
     * Attempts to update the user vote on the remote server.
     * This does not gracefully handle connectivity issues: Failed API calls will be ignored.
     * @param isAddition Whether the vote should be added or removed.
     */
    @WorkerThread
    private suspend fun favoriteAPIUpdate(itemID: String, isAddition: Boolean) {
        try {
            if (isAddition) {
                api.like(itemID).await()
            } else {
                api.dislike(itemID).await()
            }
        } catch (t: IOException) {
            MainActivity.instance.runOnUiThread {
                Toast.makeText(MainActivity.instance, R.string.network_error, Toast.LENGTH_LONG).show()
            }
        } catch (t: Throwable) {
            println("Error occurred during API update: $t")
        }

        // Refresh DB
        loadAll()
    }

    /**
     * Toggles the favorite state of a museum object.
     * The API and database updates are performed asynchronously.
     */
    fun favoriteToggle(itemID: String) {
        val localVote = dao.getLocalState(itemID)

        localVote.observeForever(object : Observer<MuseumObjectLocalState?> {
            override fun onChanged(oldVoteObject: MuseumObjectLocalState?) {
                val oldVote = oldVoteObject?.isFavorite ?: false
                val newVote = !oldVote

                GlobalScope.launch {
                    favoriteAPIUpdate(itemID, newVote)
                    dao.insertLocalState(MuseumObjectLocalState(itemID, newVote))
                }

                localVote.removeObserver(this)
            }
        })
    }

    companion object {
        val instance: MuseumRepository by lazy { MuseumRepository() }
    }

    init {
        GlobalScope.launch { loadAll() }
    }
}