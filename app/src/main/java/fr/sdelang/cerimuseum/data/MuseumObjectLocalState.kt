package fr.sdelang.cerimuseum.data

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
        tableName = "museum_object_local_state",
        indices = [Index(value = ["itemID"], unique = true)]
)
data class MuseumObjectLocalState(
        @PrimaryKey
        var itemID: String = "",
        var isFavorite: Boolean = false
)