package fr.sdelang.cerimuseum.data.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import fr.sdelang.cerimuseum.MainActivity
import fr.sdelang.cerimuseum.data.MuseumObjectInfo
import fr.sdelang.cerimuseum.data.MuseumObjectLocalState
import fr.sdelang.cerimuseum.data.MuseumObjectPopularity

/**
 * Implements a singleton RoomDatabase for museum objects.
 */
@Database(entities = [
        MuseumObjectInfo::class,
        MuseumObjectPopularity::class,
        MuseumObjectLocalState::class
    ], version = 1, exportSchema = false)
@TypeConverters(MuseumObjectConverters::class)
abstract class MuseumRoomDatabase : RoomDatabase() {
    abstract fun getMuseumDAO(): MuseumDAO

    companion object {
        val instance: MuseumRoomDatabase by lazy {
            Room.databaseBuilder(MainActivity.instance, MuseumRoomDatabase::class.java, "museum_objects")
                    .build()
        }
    }
}