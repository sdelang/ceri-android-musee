package fr.sdelang.cerimuseum.data

import androidx.room.Embedded
import androidx.room.Relation

data class MuseumObject(
        @Embedded
        val info: MuseumObjectInfo,

        @Relation(parentColumn = "itemID", entityColumn = "itemID")
        val popularity: MuseumObjectPopularity?,

        @Relation(parentColumn = "itemID", entityColumn = "itemID")
        var localVote: MuseumObjectLocalState?
)