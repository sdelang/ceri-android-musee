package fr.sdelang.cerimuseum.data.database

import androidx.room.TypeConverter
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

/**
 * Implements Room object converters used for persistence of the MuseumObjectInfo class.
 */
class MuseumObjectConverters {
    private val moshi = Moshi.Builder().build()

    private val stringListAdapter: JsonAdapter<List<String>> = moshi.adapter(
            Types.newParameterizedType(List::class.java, String::class.java))

    @TypeConverter
    fun serializeStringList(list: List<String>?): String {
        return stringListAdapter.toJson(list.orEmpty())
    }

    @TypeConverter
    fun deserializeStringList(list: String): List<String> {
        return stringListAdapter.fromJson(list).orEmpty()
    }

    private val pictureMapAdapter: JsonAdapter<Map<String, String>> = moshi.adapter(
            Types.newParameterizedType(Map::class.java, String::class.java, String::class.java))

    @TypeConverter
    fun serializePictureMap(map: Map<String, String>?): String {
        return pictureMapAdapter.toJson(map.orEmpty())
    }

    @TypeConverter
    fun deserializePictureMap(map: String): Map<String, String> {
        return pictureMapAdapter.fromJson(map).orEmpty()
    }

    private val intListAdapter: JsonAdapter<List<Integer>> = moshi.adapter(
            Types.newParameterizedType(List::class.java, Integer::class.java))

    @TypeConverter
    fun serializeIntList(list: List<Integer>?): String {
        return intListAdapter.toJson(list.orEmpty())
    }

    @TypeConverter
    fun deserializeIntList(list: String): List<Integer> {
        return intListAdapter.fromJson(list).orEmpty()
    }
}