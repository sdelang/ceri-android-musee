package fr.sdelang.cerimuseum.data

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
@Entity(
        tableName = "museum_objects",
        indices = [Index(value = ["itemID"], unique = true)]
)
data class MuseumObjectInfo(
        @PrimaryKey
        var itemID: String = "",
        var description: String = "",
        var categories: List<String> = ArrayList(),
        var pictures: Map<String, String>? = HashMap(),
        var working: Boolean? = null,
        var year: Int? = null,
        var name: String = "",
        var technicalDetails: List<String>? = null,
        var brand: String? = null,
        var timeFrame: List<Int> = ArrayList()
)