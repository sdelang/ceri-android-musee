package fr.sdelang.cerimuseum.data

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
        tableName = "museum_object_popularity",
        indices = [Index(value = ["itemID"], unique = true)]
)
data class MuseumObjectPopularity(
        @PrimaryKey
        var itemID: String = "",
        var popularity: Int = 0
)