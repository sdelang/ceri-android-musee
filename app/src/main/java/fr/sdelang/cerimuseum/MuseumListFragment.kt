package fr.sdelang.cerimuseum

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.view.children
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import fr.sdelang.cerimuseum.MuseumListViewModel.SecondarySort.*
import fr.sdelang.cerimuseum.MuseumListViewModel.SortMethod
import fr.sdelang.cerimuseum.data.MuseumRepository
import kotlinx.android.synthetic.main.fragment_object_list.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class MuseumListFragment : Fragment(R.layout.fragment_object_list) {
    lateinit var adapter: MuseumRecyclerAdapter
    lateinit var viewModel: MuseumListViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(MuseumListViewModel::class.java)

        // Recycler view setup
        adapter = MuseumRecyclerAdapter()
        museumObjects.layoutManager = LinearLayoutManager(activity)
        museumObjects.adapter = adapter

        // Swipe-to-refresh
        museumObjectsSwipeRefresh.setOnRefreshListener {
            GlobalScope.launch {
                MuseumRepository.instance.loadAll()
                museumObjectsSwipeRefresh.isRefreshing = false
            }
        }

        // TODO: drawer expand button

        // Search text edit
        // Handle filter text changes
        museumObjectsSearch.setOnQueryTextListener(object :
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.filterText = query.orEmpty()
                // We return false as we want to let the view close itself
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                viewModel.filterText = newText.orEmpty()
                // We return false as we only want to intercept output
                return false
            }
        })

        museumObjectsSearch.onActionViewExpanded()
        museumObjectsSearch.clearFocus()
        museumObjectsSearch.queryHint = "Nom ou marque"

        // Sort button menu
        museumObjectsSortButton.setOnClickListener {
            val popup = PopupMenu(view.context, it)
            popup.inflate(R.menu.sort_menu)

            for (item in popup.menu.children) {
                item.isChecked = when (item.itemId) {
                    R.id.sortMenuShowCategories -> viewModel.sortMethod.categoryView
                    R.id.sortMenuFavoritesFirst -> viewModel.sortMethod.favoritesFirst
                    /*R.id.sortMenuAlphabeticalOrder -> viewModel.sortMethod.secondarySort == NAME_ASC
                    R.id.sortMenuDate -> viewModel.sortMethod.secondarySort == YEAR_DESC
                    R.id.sortMenuPopularity -> viewModel.sortMethod.secondarySort == POPULARITY_DESC*/
                    else -> false
                }
            }

            popup.setOnMenuItemClickListener { item ->
                val oldSortMethod = viewModel.sortMethod
                viewModel.sortMethod = when (item!!.itemId) {
                    R.id.sortMenuShowCategories -> SortMethod(oldSortMethod.favoritesFirst, oldSortMethod.secondarySort, !oldSortMethod.categoryView)
                    R.id.sortMenuFavoritesFirst -> SortMethod(!oldSortMethod.favoritesFirst, oldSortMethod.secondarySort, oldSortMethod.categoryView)
                    R.id.sortMenuAlphabeticalOrder -> SortMethod(oldSortMethod.favoritesFirst, NAME_ASC, oldSortMethod.categoryView)
                    R.id.sortMenuDate -> SortMethod(oldSortMethod.favoritesFirst, YEAR_DESC, oldSortMethod.categoryView)
                    R.id.sortMenuPopularity -> SortMethod(oldSortMethod.favoritesFirst, POPULARITY_DESC, oldSortMethod.categoryView)
                    else -> oldSortMethod
                }

                true
            }

            popup.show()
        }

        setupObservers()
    }

    private fun setupObservers() {
        viewModel.objects.observe(viewLifecycleOwner) {
            adapter.categoryAnchors = viewModel.categoryAnchors
            adapter.objects = it
        }
    }
}