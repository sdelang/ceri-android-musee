package fr.sdelang.cerimuseum

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.lifecycle.observe
import androidx.navigation.fragment.navArgs
import com.squareup.picasso.Picasso
import fr.sdelang.cerimuseum.data.MuseumRepository
import kotlinx.android.synthetic.main.fragment_museum_detail.*

class MuseumDetailFragment : Fragment() {
    private val args: MuseumDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_museum_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val obj = MuseumRepository.instance.dao.getObject(args.itemID)

        obj.observe(viewLifecycleOwner) {
            val context: Context = MainActivity.instance
            val repo = MuseumRepository.instance

            Picasso.get().load(repo.getThumbnailURL(it.info.itemID)).into(objectDetailThumbnail)

            // Set object name
            if (!it.info.brand.isNullOrBlank()) {
                objectDetailName.text = context.getString(R.string.detail_title_brand,
                        it.info.name,
                        it.info.brand!!
                )
            } else {
                objectDetailName.text = context.getString(R.string.detail_title_no_brand,
                        it.info.name)
            }

            // Set working state
            objectDetailWorking.text = if (it.info.working == true)
                context.getString(R.string.detail_working) else
                context.getString(R.string.detail_not_working)

            // Set category detail
            objectDetailCategory.text = context.getString(R.string.detail_tags,
                    it.info.categories.joinToString(separator = ", "))

            // Set description
            objectDetailDescription.text = context.getString(R.string.detail_description,
                    it.info.description)

            // Set time frame
            if (!it.info.timeFrame.isNullOrEmpty()) {
                objectDetailTimeFrame.visibility = View.VISIBLE
                objectDetailTimeFrame.text = context.getString(R.string.detail_timeframe,
                    it.info.timeFrame.joinToString(separator =  ", "))
            } else {
                objectDetailTimeFrame.visibility = View.GONE
            }

            // Set year
            if (it.info.year != null) {
                objectDetailReleaseDate.visibility = View.VISIBLE
                objectDetailReleaseDate.text = context.getString(R.string.detail_release_year,
                    it.info.year!!)
            } else {
                objectDetailReleaseDate.visibility = View.GONE
            }

            // Set technical details
            if (!it.info.technicalDetails.isNullOrEmpty()) {
                objectDetailTechnicalDetails.visibility = View.VISIBLE
                objectDetailTechnicalDetails.text = context.getString(R.string.detail_technical_details,
                    it.info.technicalDetails!!.joinToString(prefix = "• ", separator = "\n• "))
            } else {
                objectDetailTechnicalDetails.visibility = View.GONE
            }

            // Set favourite button state
            objectDetailFavouriteButton.setOnClickListener { _ ->
                repo.favoriteToggle(it.info.itemID)
            }
            ViewCompat.setBackgroundTintList(objectDetailFavouriteButton,
                    ContextCompat.getColorStateList(context,
                            if (it.localVote?.isFavorite == true) R.color.purple_500
                            else R.color.gray_unselected
                    )
            )

            objectDetailPictureLayout.removeAllViews()

            objectDetailPopularity.text = context.getString(R.string.detail_popularity,
                    it.popularity?.popularity ?: 0)

            // Load picture views
            for ((imageID, caption) in it.info.pictures.orEmpty()) {
                // We want to attach the new picture to the picture layout (root).
                // We could do so by specifying `true` for the `attachToRoot` parameter.
                // However, if we do that, `inflate` will return the root for some reason.
                // If we don't, it will return the inflated view.
                // As a result, we inflate the view and attach it to the picture layout only after.
                val pictureView = layoutInflater.inflate(
                        R.layout.museum_object_picture,
                        objectDetailPictureLayout,
                        false) as CardView

                objectDetailPictureLayout.addView(pictureView)

                val imageView: ImageView = pictureView.findViewById(R.id.objectPicture)
                val captionView: TextView = pictureView.findViewById(R.id.objectPictureCaption)

                val imageURL = repo.getPictureURL(it.info.itemID, imageID)

                imageView.setOnClickListener {  _ ->
                    val intent = Intent()
                    intent.action = Intent.ACTION_VIEW
                    intent.data = Uri.parse(imageURL)
                    startActivity(intent)
                }

                Picasso.get()
                        .load(imageURL)
                        .into(imageView)

                if (!caption.isEmpty()) {
                    captionView.text = caption
                } else {
                    captionView.visibility = View.GONE
                }
            }
        }
    }
}