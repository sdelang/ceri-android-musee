package fr.sdelang.cerimuseum

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import fr.sdelang.cerimuseum.data.MuseumObject
import fr.sdelang.cerimuseum.data.MuseumRepository
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class MuseumListViewModel(application: Application) : AndroidViewModel(application) {
    data class SortMethod(
        val favoritesFirst: Boolean = true,
        val secondarySort: SecondarySort = SecondarySort.NAME_ASC,
        val categoryView: Boolean = false
    )

    enum class SecondarySort {
        NAME_ASC,
        POPULARITY_DESC,
        YEAR_DESC
    }

    private val repo = MuseumRepository.instance
    private val unsortedObjects = repo.getObjects()
    val objects : MutableLiveData<List<MuseumObject>> = MutableLiveData()
    var categoryAnchors = getDefaultCategoryAnchors()

    private var sortComparables : List<(MuseumObject) -> Comparable<*>> = ArrayList()
    var sortMethod = SortMethod()
        set(value) {
            field = value
            updateSortMethod()
        }

    var filterText = ""
        set(value) {
            field = value
            updateObjects(unsortedObjects.value)
        }

    private fun updateSortMethod() {
        val newSortMethod: ArrayList<(MuseumObject) -> Comparable<*>> = ArrayList()

        if (sortMethod.favoritesFirst) {
            newSortMethod.add { !(it.localVote?.isFavorite ?: false) }
        }

        when (sortMethod.secondarySort) {
            SecondarySort.NAME_ASC -> newSortMethod += { it.info.name }
            SecondarySort.YEAR_DESC -> newSortMethod += { -(it.info.year ?: 0) }
            SecondarySort.POPULARITY_DESC -> newSortMethod += { -(it.popularity?.popularity ?: 0) }
        }

        sortComparables = newSortMethod
        updateObjects(unsortedObjects.value)
    }

    private fun shouldShowItem(objectInfo: MuseumObject): Boolean {
        return filterText.isBlank()
                || objectInfo.info.name.contains(filterText, ignoreCase = true)
                || objectInfo.info.brand?.contains(filterText, ignoreCase = true) ?: false
    }

    private fun getCategories(objects: List<MuseumObject>): Map<String, List<MuseumObject>> {
        val categoryFrequencies = TreeMap<String, ArrayList<MuseumObject>>()

        for (obj in objects) {
            for (category in obj.info.categories) {
                categoryFrequencies.getOrPut(category) { ArrayList() }.add(obj)
            }
        }

        return categoryFrequencies
    }

    private fun updateObjects(unsorted: List<MuseumObject>?) {
        // TODO: default to name asc sorting, don't allow no sorting
        //       because this messes up with filtering and category view otherwise
        if (sortComparables.isNotEmpty()) {
            val sorted: List<MuseumObject> = unsorted
                .orEmpty()
                .filter { shouldShowItem(it) }
                .sortedWith(compareBy(*sortComparables.toTypedArray()))

            if (sortMethod.categoryView) {
                val newCategoryAnchors = HashMap<Int, String>()
                val categorizedList = ArrayList<MuseumObject>()

                for ((category, objectsInCategory) in getCategories(sorted)) {
                    newCategoryAnchors[categorizedList.size] = category
                    categorizedList += objectsInCategory
                }

                categoryAnchors = newCategoryAnchors
                objects.postValue(categorizedList)
            } else {
                categoryAnchors = getDefaultCategoryAnchors()
                objects.postValue(sorted)
            }
        } else {
            objects.postValue(unsorted)
        }
    }

    /**
     * Returns a map with a single category anchor at position 0 describing "all categories".
     * To use outside of the category view.
     */
    private fun getDefaultCategoryAnchors(): Map<Int, String> {
        val anchors = HashMap<Int, String>()
        anchors[0] = MainActivity.instance.getString(R.string.categories_all)
        return anchors
    }

    init {
        unsortedObjects.observeForever { updateObjects(it) }
        updateSortMethod()
    }
}